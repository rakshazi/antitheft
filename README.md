# Antitheft
[![GoDoc](https://img.shields.io/badge/godoc-reference-informational.svg?style=for-the-badge)](https://godoc.org/gitlab.com/rakshazi/antitheft)
[![Patreon](https://img.shields.io/badge/donate-patreon-orange.svg?style=for-the-badge)](https://patreon.com/rakshazi)
[![PayPal](https://img.shields.io/badge/donate-paypal-blue.svg?style=for-the-badge)](https://paypal.me/rakshazi)
[![Liberapay](https://img.shields.io/badge/donate-liberapay-yellow.svg?style=for-the-badge)](https://liberapay.com/rakshazi)

That tools will help you to find stolen laptop. It collects following information:

* Location information from [here.com](https://here.com) - GPS location by nearest WiFi networks
* IP address and location information from [ifconfig.co](https://ifconfig.co) - IP, Provider, ASN, Hostname and GPS info by IP address
* Screenshots - 1 screenshot per display
* Processes - list of running processes (system/user)

and sends it to your email address.

Configuration ([antitheft.json.dist](config.json.dist)), save it to `$HOME/.config/antitheft.json`:

```json
{
    "here": {
        "apikey": "YOUR here.com API Key. It's optional"
    },
    "mail": {
        "host": "smtp.yourmailprovider.com",
        "port": 587,
        "user": "user@yourmailprovider.com",
        "password": "your_password",
        "subject": "Laptop information",
        "recipient": "where-should-i-send-email@yourmailprovider.com"
    }
}
```

## Install

### Binary

1. Download [from releases page](https://gitlab.com/rakshazi/antitheft/-/releases)
2. Save in preferred directory (don't forget about `$PATH`, so the best solution is to place `/bin/any-name-that-you-want`)
3. `chmod +x /path/to/binary`

### Golang

1. `go get gitlab.com/rakshazi/antitheft`
2. `go install gitlab.com/rakshazi/antitheft`

## Usage

You may use it as you want, but here is list of examples

### cron, hourly

1. `crontab -e`
2. Add `0 * * * * /path/to/binary`

After that, you will receive report to your email, every hour
