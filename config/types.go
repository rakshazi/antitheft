package config

// Config is config, Nuff said.
type Config struct {
	Here Here `json:"here"`
	Mail Mail `json:"mail"`
}

// Here - config for here.com API
type Here struct {
	Key string `json:"apikey"`
}

// Mail - email configuration
type Mail struct {
	Host      string `json:"host"`
	Port      int    `json:"port"`
	User      string `json:"user"`
	Password  string `json:"password"`
	Subject   string `json:"subject"`
	Recipient string `json:"recipient"`
}
