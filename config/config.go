package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// New config
func New(file string) (Config, error) {
	config := Config{}

	if _, err := os.Stat(file); os.IsNotExist(err) {
		return Config{}, fmt.Errorf("file not found")
	}

	data, err := ioutil.ReadFile(file)
	if err != nil {
		return Config{}, err
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		return Config{}, err
	}
	return config, nil
}

// GetHome - returns user home directory
func GetHome() string {
	home := os.Getenv("XDG_CONFIG_HOME")
	if home != "" {
		return home
	}
	return os.Getenv("HOME")
}
