package ifconfig

// Response - ifconfig.co response
type Response struct {
	IP        string  `json:"ip"`
	Country   string  `json:"country"`
	City      string  `json:"city"`
	Hostname  string  `json:"hostname"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Provider  string  `json:"asn_org"`
	ASN       string  `json:"asn"`
}

// Info - parsed ifconfig response
type Info struct {
	GPS     `json:"gps"`
	Network `json:"network"`
	Address `json:"address"`
}

// GPS - ifconfig GPS data
type GPS struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lng"`
}

// Network - ifconfig.co network data
type Network struct {
	IP       string `json:"ip"`
	Hostname string `json:"hostname"`
	Provider string `json:"provider"`
	ASN      string `json:"asn"`
}

// Address - ifconfig.co address data
type Address struct {
	Country string `json:"country"`
	City    string `json:"city"`
}
