package ifconfig

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// GetIPInfo - get information by IP
func GetIPInfo() (Info, error) {
	response, err := http.Get("https://ifconfig.co/json")
	if err != nil {
		return Info{}, err
	}
	data, _ := ioutil.ReadAll(response.Body)
	var ifconfigResponse Response
	json.Unmarshal(data, &ifconfigResponse)
	return Info{
		GPS: GPS{
			Latitude:  ifconfigResponse.Latitude,
			Longitude: ifconfigResponse.Longitude,
		},
		Address: Address{
			Country: ifconfigResponse.Country,
			City:    ifconfigResponse.City,
		},
		Network: Network{
			IP:       ifconfigResponse.IP,
			ASN:      ifconfigResponse.ASN,
			Hostname: ifconfigResponse.Hostname,
			Provider: ifconfigResponse.Provider,
		},
	}, nil
}
