package screenshot

import (
	"fmt"
	"image/jpeg"
	"log"
	"os"

	"github.com/kbinani/screenshot"
)

var files []string

// GetScreenshots - capture screenshots of all active displays
func GetScreenshots() []string {
	displays := screenshot.NumActiveDisplays()
	for i := 0; i < displays; i++ {
		bounds := screenshot.GetDisplayBounds(i)
		img, err := screenshot.CaptureRect(bounds)
		if err != nil {
			log.Println("[screenshot]", err)
		}
		filename := fmt.Sprintf("/tmp/%d_%dx%d.jpg", i, bounds.Dx(), bounds.Dy())
		file, _ := os.Create(filename)
		defer file.Close()
		jpeg.Encode(file, img, &jpeg.Options{Quality: 80})
		files = append(files, filename)
	}

	return files
}
