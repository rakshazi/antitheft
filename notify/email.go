package notify

import (
	"log"
	"net/smtp"
	"os"
	"path/filepath"
	"strconv"

	"github.com/domodwyer/mailyak"
	"gitlab.com/rakshazi/antitheft/config"
)

// Email - send email with collected data
func Email(cfg config.Mail, text string, attachments []string) error {
	auth := smtp.PlainAuth("", cfg.User, cfg.Password, cfg.Host)
	mail := mailyak.New(cfg.Host+":"+strconv.Itoa(cfg.Port), auth)
	mail.To(cfg.Recipient)
	mail.From(cfg.User)
	mail.Subject(cfg.Subject)
	mail.Plain().Set(text)
	for _, filename := range attachments {
		file, err := os.Open(filename)
		if err != nil {
			log.Println("[notify/email]", filename, err)
		} else {
			mail.Attach(filepath.Base(filename), file)
		}
	}
	err := mail.Send()
	for _, filename := range attachments {
		os.Remove(filename)
	}
	return err
}
