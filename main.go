package main

import (
	"log"
	"path/filepath"
	"strconv"

	"gitlab.com/rakshazi/antitheft/config"
	"gitlab.com/rakshazi/antitheft/here"
	"gitlab.com/rakshazi/antitheft/ifconfig"
	"gitlab.com/rakshazi/antitheft/notify"
	"gitlab.com/rakshazi/antitheft/process"
	"gitlab.com/rakshazi/antitheft/screenshot"
)

func main() {
	log.Println("Starting application...")
	configfile := filepath.Join(config.GetHome(), ".config", "antitheft.json")
	cfg, err := config.New(configfile)
	if err != nil {
		log.Println("[config]", err)
		return
	}

	log.Println("Loading list of system processes...")
	processes, err := process.GetProcesses()
	if err != nil {
		log.Println("[processes]", err)
	}

	log.Println("Capturing screenshots...")
	screenshots := screenshot.GetScreenshots()

	log.Println("Loading location from here.com...")
	location, err := here.GetLocation(cfg.Here.Key)
	if err != nil {
		log.Println("[here.com]", err)
	}

	log.Println("Loading information from ifconfig.co")
	ipinfo, err := ifconfig.GetIPInfo()
	if err != nil {
		log.Println("[ifconfig.co]", err)
	}

	log.Println("Preparing email...")
	text := toString(location, ipinfo, processes)
	err = notify.Email(cfg.Mail, text, screenshots)
	if err != nil {
		log.Println("[notify/email]", err)
	}
	log.Println("done")
}

// Export report to string
func toString(location here.Location, ipinfo ifconfig.Info, processes []string) string {
	text := "============ LOCATION ============\n"
	text += "Country: " + ipinfo.Address.Country + "\n"
	text += "City: " + ipinfo.Address.City + "\n"
	text += "Address: " + location.Address.Street + "\n"
	text += "GPS: lat=" + strconv.FormatFloat(location.GPS.Latitude, 'f', 6, 64)
	text += ", lng=" + strconv.FormatFloat(location.GPS.Longitude, 'f', 6, 64) + ", accuracy=" + string(location.GPS.Accuracy) + "m \n"
	text += "\n"
	text += "============ NETWORK ============\n"
	text += "Provider: " + ipinfo.Network.Provider + "\n"
	text += "ASN: " + ipinfo.Network.ASN + "\n"
	text += "Hostname: " + ipinfo.Network.Hostname + "\n"
	text += "IP: " + ipinfo.Network.IP + "\n"
	text += "\n"
	text += "============ SYSTEM ============\n"
	text += "Screeshots: check attachments \n"
	text += "Running processes:\n"
	for _, item := range processes {
		text += item + "\n"
	}
	text += "\n\nEnd Of Report"
	return text
}
