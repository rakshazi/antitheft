package process

import (
	"strings"

	"github.com/mitchellh/go-ps"
)

var processes []string

func GetProcesses() ([]string, error) {
	list, err := ps.Processes()
	if err != nil {
		return processes, err
	}

	for _, item := range list {
		slice := strings.Split(item.Executable(), "/")
		processes = appendMissing(processes, slice[0])
	}

	return processes, nil
}

// AppendMissing only if missing
func appendMissing(slice []string, newItem string) []string {
	for _, item := range slice {
		if item == newItem {
			return slice
		}
	}
	return append(slice, newItem)
}
