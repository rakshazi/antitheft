package here

// Calculated location information
type Location struct {
	GPS     GPS     `json:"gps"`
	Address Address `json:"address"`
}
type GPS struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lng"`
	Accuracy  int     `json:"accuracy"`
}
type Address struct {
	Street string `json:"street"`
}

// Platform - api config
type Platform struct {
	APIKey string `json:"apiKey"`
}

// WlanInfo - wifi networks info
type WlanInfo struct {
	BSSID string `json:"mac"`
	Power int    `json:"powrx,omitempty"`
}

// AccessPoints - list of access points
type AccessPoints struct {
	Wlan []WlanInfo `json:"wlan"`
}

// PositionResponse - here API position response
type PositionResponse struct {
	Location struct {
		Latitude  float64 `json:"lat"`
		Longitude float64 `json:"lng"`
		Accuracy  int     `json:"accuracy"`
	} `json:"location"`
}

// GeocoderResponse - here API geocoder response
type GeocoderResponse struct {
	Response struct {
		MetaInfo struct {
			TimeStamp string `json:"TimeStamp"`
		} `json:"MetaInfo"`
		View []struct {
			Result []struct {
				MatchLevel string `json:"MatchLevel"`
				Location   struct {
					Address struct {
						Label       string `json:"Label"`
						Country     string `json:"Country"`
						State       string `json:"State"`
						County      string `json:"County"`
						City        string `json:"City"`
						District    string `json:"District"`
						Street      string `json:"Street"`
						HouseNumber string `json:"HouseNumber"`
						PostalCode  string `json:"PostalCode"`
					} `json:"Address"`
				} `json:"Location"`
			} `json:"Result"`
		} `json:"View"`
	} `json:"Response"`
}
