package here

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

// GetLocation - get device location
func GetLocation(key string) (Location, error) {
	var accessPoints AccessPoints

	if key == "" {
		return Location{}, errors.New("API Key is empty, cannot get location")
	}

	platform := Platform{APIKey: key}
	accessPoints, err := GetAccessPoints()
	if err != nil {
		return Location{}, err
	}

	payload, err := json.Marshal(accessPoints)
	if err != nil {
		return Location{}, err
	}

	positionResponse, err := platform.GetPosition(payload)
	if err != nil {
		return Location{}, err
	}
	geocoderResponse, err := platform.ReverseGeocode(positionResponse)
	if err != nil {
		return Location{GPS: positionResponse.Location}, err
	}

	return Location{
		GPS:     positionResponse.Location,
		Address: Address{Street: geocoderResponse.Response.View[0].Result[0].Location.Address.Label},
	}, nil
}

// GetAccessPoints - get list of WLAN access points
func GetAccessPoints() (AccessPoints, error) {
	iwlistCmd := exec.Command("iwlist", "wlp2s0", "scan")
	iwlistCmdOutput, err := iwlistCmd.Output()
	if err != nil {
		return AccessPoints{}, err
	}
	lines := strings.Split(string(iwlistCmdOutput), "\n")
	var wlans []WlanInfo
	for _, line := range lines {
		columns := strings.Fields(line)
		if len(columns) == 5 {
			match, _ := regexp.MatchString("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$", columns[4])
			if match == true {
				wlan := WlanInfo{
					BSSID: strings.Replace(columns[4], ":", "-", -1),
				}
				wlans = append(wlans, wlan)
			}
		}
	}

	return AccessPoints{Wlan: wlans}, nil
}

// GetPosition - get position by list of access points
func (platform *Platform) GetPosition(payload []byte) (PositionResponse, error) {
	endpoint, _ := url.Parse("https://pos.ls.hereapi.com/positioning/v1/locate")
	queryParams := endpoint.Query()
	queryParams.Set("apiKey", platform.APIKey)
	queryParams.Set("confidence", "50")
	queryParams.Set("fallback", "singleWifi")
	endpoint.RawQuery = queryParams.Encode()
	response, err := http.Post(endpoint.String(), "application/json", bytes.NewBuffer(payload))
	if err != nil {
		return PositionResponse{}, err
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		var positionResponse PositionResponse
		json.Unmarshal(data, &positionResponse)
		return positionResponse, nil
	}
}

// ReverseGeocode - get address by GetPosition() response
func (platform *Platform) ReverseGeocode(position PositionResponse) (GeocoderResponse, error) {
	endpoint, _ := url.Parse("https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json")
	queryParams := endpoint.Query()
	queryParams.Set("apiKey", platform.APIKey)
	queryParams.Set("mode", "retrieveAddresses")
	queryParams.Set("prox", strconv.FormatFloat(position.Location.Latitude, 'f', -1, 64)+","+strconv.FormatFloat(position.Location.Longitude, 'f', -1, 64))
	endpoint.RawQuery = queryParams.Encode()
	response, err := http.Get(endpoint.String())
	if err != nil {
		return GeocoderResponse{}, err
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		var geocoderResponse GeocoderResponse
		json.Unmarshal(data, &geocoderResponse)
		return geocoderResponse, nil
	}
}
